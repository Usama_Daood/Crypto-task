import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CurrencyList from '../screens/currencyLists';

export default function App({ navigation }) {
  const Stack = createStackNavigator();

  const [state, dispatch] = React.useReducer( );

MyStack = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={CurrencyList}
          options={{ title: 'CurrencyList' }}
        />
      </Stack.Navigator>
  );
}
}
