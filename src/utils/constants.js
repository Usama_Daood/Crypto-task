const colors = {
  GREEN: 'rgb(3, 196, 1)',
  PURPLE: 'rgb(156, 117, 184)',
  YELLOW: '#ffb700',
  ORANGE: 'rgb(243, 166, 95)',
  GREY: 'black',
  LIGHT_BLUE: 'rgb(205, 221, 218)',
  DARK_BLUE: 'rgb(41, 83, 100)',
  RED: '#f02401',
  WHITE: '#ffffff'
};

module.exports = {
  items,
  colors,


};
