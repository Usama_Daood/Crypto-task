import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { View, TextInput,Button, StyleSheet
  ,  SafeAreaView,AsyncStorage, Text, TouchableOpacity, Icon, Image
} from "react-native";

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import CurrencyList from './screens/currencyLists'
import AddCurrency from './screens/AddCurrency'


const Stack = createStackNavigator();
export const AuthContext = React.createContext();
import { openDatabase } from 'react-native-sqlite-storage';
 
var db = openDatabase({ name: 'Crypto.db' });
export default function App({ navigation }) {
  console.disableYellowBox = true;

  useEffect(() => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='TABLE' AND name='Crypto_Table'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS Crypto_Table', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS Crypto_Table(crypto_id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(30), value VARCHAR(30))',
              []
            );
          }
        }
      );
    })

  }, []);
  
  return (
  <NavigationContainer>

        <Stack.Navigator>
            <Stack.Screen name="CurrencyList" component={CurrencyList} options={{headerShown: false}}/>
            <Stack.Screen name="AddCurrency" component={AddCurrency} options={{title: 'Back to list'}}/>
          </Stack.Navigator>

  </NavigationContainer>
  );
}
