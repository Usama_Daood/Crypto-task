import React, { Component } from 'react';
import { View, TextInput,Button, StyleSheet
  ,  SafeAreaView,ScrollView, Text, TouchableOpacity, Icon, Image
} from "react-native";
import functions from '../utils/functions';
var SQLite = require('react-native-sqlite-storage');
var db;
export default class currencyList extends Component<{}> {

  async _handleSignup() {
      this.props.navigation.navigate('AddCurrency');
  }

  async componentDidMount(){
    console.log("Component did mount called");
    db = SQLite.openDatabase({name: 'test.db', createFromLocation: '~test.db'});
    this._displayValueDB();

  }

  async _displayValueDB() {

    try{ 
      Alert.alert('Currency Name ', );
    }catch(err){
      console.log("Log. ok ", err);
    }
  }


  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={styles.mainContainer}  keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-start' }}>
        <View style={{flexDirection: 'row', backgroundColor: '#385775', height: 150, justifyContent: 'space-between', alignContent: 'center', alignItems:'center'}}>
        <Text style={{marginLeft: 25}}><Text style={{fontWeight: 'bold', color: '#fff', fontSize: 17}}>CryptoTracker Pro </Text> </Text>
        
        <Image style={{marginRight: 25,width: 40, height: 40 , borderRadius:25,}} source={require('../assets/images/ic_user.jpeg')} />
        </View>
        {/* End top Header */}
        <View style={{flexDirection: 'column', marginBottom: 24,}}>
          <View style={{flex: 1 ,flexDirection: 'row', marginLeft: 30, marginRight: 30 , borderBottomColor: '#EEEEEE', borderBottomWidth: 0.5, padding: 14}}>
          <Image style={{width: 30, height: 30, alignContent: 'center', alignItems: 'center'}} source={require('../assets/images/ic_bitcoin.png')} />
        <Text style={{marginLeft: 10}}><Text style={{fontWeight: 'bold'}}>Bitcoin </Text>{'\n'}BTC </Text>

        <View style={{flex: 1,alignContent: 'flex-end', alignItems: 'flex-end',}}>
        <Text style={{alignContent: 'flex-end',  textAlign: 'center'}}><Text style={{fontWeight: 'bold'}}>$7215.68 </Text>{'\n'} <Text style={{color: 'blue'}}> ↖ 1.82% </Text>  </Text>
        </View>
        </View>
         {/* end first Row */}
        
         <View style={{flex: 1 ,flexDirection: 'row', marginLeft: 30, marginRight: 30, borderBottomColor: '#EEEEEE', borderBottomWidth: 0.5, padding: 14}}>
          <Image style={{width: 30, height: 30, alignContent: 'center', alignItems: 'center', }} source={require('../assets/images/ic_eth.png')} />
        <Text style={{marginLeft: 10}}><Text style={{fontWeight: 'bold'}}>Ethereum </Text>{'\n'}ETH </Text>

        <View style={{flex: 1,alignContent: 'flex-end', alignItems: 'flex-end',}}>
        <Text style={{alignContent: 'flex-end',  textAlign: 'center'}}><Text style={{fontWeight: 'bold'}}>$146.83 </Text>{'\n'} <Text style={{color: 'blue'}}> ↖ 1.46% </Text>  </Text>
        </View>
        </View>

        {/* End second Row */}

        <View style={{flex: 1 ,flexDirection: 'row', marginLeft: 30, marginRight: 30, borderBottomColor: '#EEEEEE', borderBottomWidth: 0.5, padding: 14}}>
          <Image style={{width: 30, height: 30, alignContent: 'center', alignItems: 'center'}} source={require('../assets/images/ic_xrp.png')} />
        <Text style={{marginLeft: 10}}><Text style={{fontWeight: 'bold'}}>XRP </Text>{'\n'}XRP </Text>

        <View style={{flex: 1,alignContent: 'flex-end', alignItems: 'flex-end',}}>
        <Text style={{alignContent: 'flex-end',  textAlign: 'center'}}><Text style={{fontWeight: 'bold'}}>$0.220568 </Text>{'\n'} <Text style={{color: 'red'}}> ↖ 2.47% </Text>  </Text>
        </View>
        </View>

        {/* End third Row */}

         </View>
         {/* end All ROws       */}
                <View style={styles.loginButton}>
                <TouchableOpacity
                  style={{
                      alignItems:'center',
                      justifyContent: 'center',
                      marginTop: 20,
                      width:380,
                      height:50,
                      borderRadius:25,
                    }}
                      onPress={() => {this._handleSignup();}}
                >
                <View style={{flexDirection: 'row'}}>
                <Text style={{textAlign: 'center', marginTop: functions.getHeight(0), color: "black", fontWeight: '200' ,fontSize: 18}}> + Add a Cryptocurrency</Text>
                </View>
                </TouchableOpacity>
                </View>
                </ScrollView>
        </SafeAreaView>
    );
    }
  }


const styles = StyleSheet.create({
mainContainer: {
  flex: 1,
  backgroundColor: '#ffffff',
},
activityIndicatorView: {
  alignItems: 'flex-start',
  justifyContent: 'center',
  borderWidth: 2,
},
logoContainer: {
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 40

},
logoImage: {
  height: 280,
  width: 380,
  marginTop: 30
},
bottomView:{
  alignItems: 'flex-start',
  justifyContent: 'flex-end',
  borderWidth: 2,
},
loginButton: {
      justifyContent: 'flex-start',
      marginVertical: 7,
      alignItems: 'center',
      flex: 1,
      flexDirection: 'column',
    },

});
