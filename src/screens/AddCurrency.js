import React, { Component } from 'react';
//import {AuthContext} from '../../App.js'
import { View, TextInput,Button, StyleSheet
  ,  SafeAreaView,ScrollView, Text, TouchableOpacity, Icon, Image, Alert
} from "react-native";
//import constants from './utils/constants';
import functions from '../utils/functions';
import { openDatabase } from 'react-native-sqlite-storage';
var SQLite = require('react-native-sqlite-storage');
var db;
export default class AddCurrency extends Component<{}> {
   state = {
  crypto_name: '',
  crypto_price: ''
  };
  async componentDidMount(){
    console.log("Component did mount called");
    db = SQLite.openDatabase({name: 'test.db', createFromLocation: '~test.db'});
  }
  async _AddValueDB() {

    try{
      db.transaction(function (tx) {
        tx.executeSql(
          'INSERT INTO Crypto_Table (name, value) VALUES (?,?)',
          [this.state.crypto_name, this.state.crypto_price],
          (tx, results) => {
            console.log('Logg. Results', results.rowsAffected);
            if (results.rowsAffected > 0) {
              Alert.alert('Data Inserted Successfully....');
            } else Alert.alert('Failed....');
          }
        );
      });
  
    }catch(err){
      console.log("Log. ok ", err);
    }
   
  }


  async _handleSignup() {
    console.log("Log. ok");
    var crypto_name = this.state.crypto_name;
    Alert.alert("Currency name ", crypto_name);
    
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={styles.mainContainer}  keyboardShouldPersistTaps='handled' showsVerticalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-start' }}>
                <View style={styles.loginButton}>
                <TouchableOpacity
                  style={{alignItems: 'flex-start', marginTop: 20, width:290, height:50, }}
                      onPress={() => {this._handleSignup();}}
                >
                <Text style={{marginTop: functions.getHeight(0), fontWeight: 'bold',color: "black", marginBottom: 16, fontSize: 25}}>Add a Cryptocurrency</Text>
                </TouchableOpacity>

                <View style={styles.inputView}>
                  <TextInput ref={(component) => (this._name = component)}
                  keyboardType={'text'}
                  returnKeyType='next'
                  style={styles.textInput}
                  onSubmitEditing={()=>{this._password.getRef().focus();}}
                  placeholder=' Use a name or ticker symbol...'
                  onChangeText={(text) => this.setState({crypto_name: text.trim()})}
                                  />
                </View>
                
                <View 
                style={{alignContent: 'flex-end', alignItems:'center', justifyContent: 'flex-end', alignItems: 'flex-end', justifyContent: 'space-between', width: 300}}>
                 <TouchableOpacity
                    style={{
                        borderWidth:1,
                        borderColor:'rgba(0,0,0,0.2)',
                        alignItems:'center',
                        justifyContent: 'center',
                        width: 150,
                        marginRight: 5,
                        height: 40,
                        backgroundColor:'#F7C132',
                      }}
                    onPress={() => {this._handleSignup();}}
                >
                <Text style={{textAlign: 'center', marginTop: functions.getHeight(0), color: "black", fontWeight: '300' ,fontSize: 18}}>Add</Text>
                </TouchableOpacity>
                </View>
                </View>
                </ScrollView>
        </SafeAreaView>
    );
    }
  }


const styles = StyleSheet.create({
mainContainer: {
  flex: 1,
  backgroundColor: '#ffffff',

},
activityIndicatorView: {
  alignItems: 'flex-start',
  justifyContent: 'center',
  borderWidth: 2,
},

bottomView:{
  alignItems: 'flex-start',
  justifyContent: 'center',
  borderWidth: 2,
},
loginButton: {
      justifyContent: 'center',
      marginVertical: 7,
      alignItems: 'center',
      flex: 1,
      flexDirection: 'column',
    },
    textInput: {
      height: 50,
      paddingLeft: 3,
      height: 40, borderColor: 'gray', width: 290 ,borderWidth: 1,
      marginBottom: 20,
      color: '#000',
      
    },

});
